"""achievements URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from WebUI.views import index, index_admin, achv_page, user_page, users_list, achvs_list, login_page, register_page, \
    logout_page, new_achv, delete_achv
from AchAPI.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index', index),
    path('index_admin', index_admin),
    path('achv_page/<int:id>', achv_page),
    path('user_page/<int:core_id>', user_page),
    path('users_list', users_list),
    path('achvs_list', achvs_list),
    path('login_page', login_page),
    path('register_page', register_page),
    path('logout_page', logout_page),
    path('new_achv', new_achv),
    path('delete_achv/<int:id>', delete_achv),
    # api
    path('api/get/achievements', get_achievements),
    path('api/get/user', get_user_achievements),
    path('api/add', add_achievement_to_user),
    path('api/add/user', add_user),
    path('api/add/achievement', add_achievement),
    path('api/del', remove_achievement_from_user),
    path('api/del/user', remove_user),
    path('api/del/achievement', remove_achievement),
    path('api/update/users', update_users)
]
