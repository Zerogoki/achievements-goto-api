import requests
from django.shortcuts import HttpResponse
from AchAPI.models import *
import json

key = open('key.txt', 'r').read()
pass_key = open('pass_api_key.txt', 'r').read()


# Create your views here.
def check_ach_request(access_key, user_id, ach_id):
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    if not user_id or not ach_id:
        return HttpResponse('invalid request', status=403)
    if not GUser.objects.filter(core_id=user_id).exists():
        user = GUser(core_id=user_id)
        user.save()
    if not Achievement.objects.filter(id=ach_id).exists():
        return HttpResponse('no such achievement', status=403)
    return None


def get_achievements(request):
    achievements_objects = list(Achievement.objects.all())
    response = {'achievements': []}
    for ach in achievements_objects:
        response['achievements'].append({'id': ach.id, 'title': ach.title, 'desc': ach.description, 'image': ach.icon})
    return HttpResponse(json.dumps(response))


def get_user_achievements(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    user_id = request.GET.get('id')
    if not user_id:
        return HttpResponse('user id is required', status=403)
    if not GUser.objects.filter(core_id=user_id).exists():
        return HttpResponse('no such user', status=403)
    user = GUser.objects.get(core_id=user_id)
    count = user.achievements.count()
    achievements = user.achievements.all()
    response = {'count': count, 'achievements': []}
    for ach in achievements:
        response['achievements'].append(ach.id)
    return HttpResponse(json.dumps(response))


def add_achievement(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    title = request.GET.get('title')
    description = request.GET.get('description', 'no description')
    icon = request.GET.get('icon', 'none')
    ach = Achievement(title=title, description=description, icon=icon)
    ach.save()
    return HttpResponse(ach.id)


def remove_achievement(request):
    access_key = request.GET.get('access_key')
    ach_id = request.GET.get('ach_id')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    if not ach_id:
        return HttpResponse('Invalid request', status=403)
    if not Achievement.objects.filter(id=ach_id).exists():
        return HttpResponse('Achievement does not exists')
    Achievement.objects.get(pk=ach_id).delete()
    return HttpResponse('Success')


def update_users(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    resp = requests.get('http://193.124.117.173:8000/api/get/all?key=%s' % pass_key)
    if resp.status_code != 200:
        return HttpResponse('pass API error', status=403)
    core_users = resp.json()
    i = 0
    for u in core_users:
        user = GUser()
        user.first_name = u.get('name', 'Unknown')
        user.last_name = u.get('surname', 'Unknown')
        user.avatar = u.get('avatar', 'none')
        if 'id' not in u:
            return HttpResponse('no user id', status=403)
        user.core_id = u.get('id')
        user.save()
        i += 1
    response = {"added": i}
    return HttpResponse(json.dumps(response))


def add_user(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    id_ = request.GET.get('id')
    first_name = request.GET.get('first_name', 'Noname')
    last_name = request.GET.get('last_name', 'Unknown')
    ava = request.GET.get('avatar', 'none')
    if not id_:
        return HttpResponse('id is required', status=403)
    if GUser.objects.filter(core_id=id_).exists():
        return HttpResponse('user exists', status=403)
    user = GUser(core_id=id_, first_name=first_name, last_name=last_name, avatar=ava)
    user.save()
    return HttpResponse('Success')


def edit_user(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    id_ = request.GET.get('id')
    first_name = request.GET.get('first_name', 'Noname')
    last_name = request.GET.get('last_name', 'Unknown')
    if not id_:
        return HttpResponse('id is required', status=403)
    if not GUser.objects.filter(core_id=id_).exists():
        return HttpResponse('no such user', status=403)
    user = GUser.objects.get(core_id=id_)
    user.first_name, user.last_name = first_name, last_name
    user.save()
    return HttpResponse('Success')


def add_achievement_to_user(request):
    access_key = request.GET.get('access_key')
    user_id = request.GET.get('user_id')
    ach_id = request.GET.get('ach_id')
    error = check_ach_request(access_key, user_id, ach_id)
    if error:
        return error
    ach = Achievement.objects.get(pk=ach_id)
    user = GUser.objects.get(core_id=user_id)
    if ach in user.achievements.all():
        return HttpResponse('user already has this achievement', status=403)
    user.achievements.add(ach)
    return HttpResponse('Success')


def remove_achievement_from_user(request):
    access_key = request.GET.get('access_key')
    user_id = request.GET.get('user_id')
    ach_id = request.GET.get('ach_id')
    error = check_ach_request(access_key, user_id, ach_id)
    if error:
        return error
    ach = Achievement.objects.get(pk=ach_id)
    user = GUser.objects.get(core_id=user_id)
    if ach not in user.achievements.all():
        return HttpResponse('user does not have this achievement', status=403)
    user.achievements.remove(ach)
    return HttpResponse('Success')


def remove_user(request):
    access_key = request.GET.get('access_key')
    if access_key != key:
        return HttpResponse('Forbidden', status=403)
    id_ = request.GET.get('id')
    if not id_:
        return HttpResponse('id is required', status=403)
    if not GUser.objects.filter(core_id=id_).exists():
        return HttpResponse('no such user', status=403)
    GUser.objects.get(core_id=id_).delete()
    return HttpResponse('Success')
