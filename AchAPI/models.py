from django.db import models


# Create your models here.

class Achievement(models.Model):
    title = models.TextField()
    description = models.TextField()
    icon = models.TextField()


class GUser(models.Model):
    core_id = models.IntegerField()
    first_name = models.TextField(default="unknown")
    last_name = models.TextField(default="unknown")
    achievements = models.ManyToManyField(Achievement)
    avatar = models.CharField(max_length=255)
