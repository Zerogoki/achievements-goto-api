from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render, redirect
from AchAPI.models import *
from django.contrib.auth.models import User
from django.template.defaulttags import register


# Create your views here.
# Achievements.objects.all()
# GUser.objects.all()
# user = GUser.objects.get(pk=id)
# user.achievements.add()

def index(request):
    all_users = GUser.objects.all()
    all_users = sorted(all_users, key=lambda user: len(user.achievements.all()))
    users = []
    for i in range(len(all_users)-1, max(-1, len(all_users)-6), -1):
        users.append(all_users[i])
    users_kols = dict()
    stra = ''
    for i in range(len(users)):
        users_kols[users[i].core_id] = str(len(users[i].achievements.all())) + '/' + str(len(Achievement.objects.all()))
        stra = stra + users[i].first_name + ' ' + users_kols[users[i].core_id]
    return render(request, 'index.html', {'users': users, 'users_kols': users_kols})


def is_auth(request):
    if request.user.is_authenticated:
        return True


def index_admin(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            users = GUser.objects.all()
            achvs = Achievement.objects.all()
            all_users = GUser.objects.all()
            all_users = sorted(all_users, key=lambda user: len(user.achievements.all()))
            users = []
            for i in range(len(all_users) - 1, max(-1, len(all_users) - 6), -1):
                users.append(all_users[i])
            users_kols = dict()
            stra = ''
            for i in range(len(users)):
                users_kols[users[i].core_id] = str(len(users[i].achievements.all())) + '/' + str(
                    len(Achievement.objects.all()))
                stra = stra + users[i].first_name + ' ' + users_kols[users[i].core_id]
            return render(request, 'index_admin.html', {'users': users, 'achvs': achvs, 'users_kols': users_kols})
        if request.method == 'POST':
            core_id = request.POST.get('user')
            achv_id = request.POST.get('achv')
            if core_id == '' or achv_id == '':
                return HttpResponse("Заполните все поля")
            user = GUser.objects.get(core_id=core_id)
            achv = Achievement.objects.get(pk=achv_id)
            user.achievements.add(achv)
            return redirect('/index_admin')
    else:
        return redirect('/login_page')


def user_page(request, core_id):
    user = GUser.objects.get(core_id=core_id)
    achvs = user.achievements.all()
    is_admin = is_auth(request)
    achs_kol = str(len(user.achievements.all())) + '/' + str(len(Achievement.objects.all()))
    return render(request, 'user_page.html', {'user': user, 'achvs': achvs, 'is_admin': is_admin, 'achs_kol': achs_kol})


def achv_page(request, id):
    achv = Achievement.objects.get(pk=id)
    users = GUser.objects.filter(achievements=achv)
    is_admin = is_auth(request)
    users_kol = len(users)
    return render(request, 'achv_page.html', {'achv': achv, 'users': users, 'is_admin': is_admin, 'users': users})


def users_list(request):
    if request.method == 'GET':
        users = GUser.objects.all()
        users_achvs = dict()
        is_admin = is_auth(request)
        for user in users:
            achvs = user.achievements.all()
            users_achvs[user] = []
            for i in range(min(len(achvs) - 1, 3), -1, -1):
                users_achvs[user].append(achvs[i])
        return render(request, 'users_list.html', {'users': users, 'users_achvs': users_achvs, 'is_admin': is_admin})
    if request.method == 'POST':
        core_id = request.POST.get('user')
        return redirect('/user_page/' + core_id)


@register.filter(name='lookup')
def lookup(value, arg):
    return value[arg]


def achvs_list(request):
    if request.method == 'GET':
        achvs = Achievement.objects.all()
        is_admin = is_auth(request)
        return render(request, 'achvs_list.html', {'achvs': achvs, 'is_admin': is_admin})
    if request.method == 'POST':
        achv_id = request.POST.get('achv_id')
        return redirect('/achv_page/' + achv_id)


def new_achv(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            return render(request, 'new_achv.html')
        if request.method == 'POST':
            achv = Achievement()
            achv.title = request.POST.get('title')
            achv.description = request.POST.get('description')
            achv.icon = request.POST.get('icon')
            if achv.title == '' or achv.description == '':
                return HttpResponse("Заполните все поля")
            achv.save()
            return redirect('/achvs_list')
    else:
        return redirect('/login_page')


def login_page(request):
    if request.method == 'GET':
        return render(request, 'login_page.html')
    if request.method == 'POST':
        username = request.POST.get('login', '')
        password = request.POST.get('password', '')

        if username == '' or password == '':
            return HttpResponse("Заполните все поля")

        # проверяем правильность логина и пароля
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/index_admin')
        else:
            return HttpResponse("Логин или пароль неверен")


def register_page(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('/index_admin')
        return render(request, 'register_page.html')
    if request.method == 'POST':
        username = request.POST.get('login', '')
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')

        if username == '' or email == '' or password == '':
            return HttpResponse("Введите все поля")
        if User.objects.filter(username=username).exists():
            return HttpResponse("Логин занят")
        user = User.objects.create_user(username, email, password)
        user.save()
        login(request, user)
        return redirect('/index_admin')


def logout_page(request):
    logout(request)
    return redirect('/index')


def delete_achv(request, id):
    achv = Achievement.objects.get(pk=id)
    Achievement.delete(achv)
    return redirect('/achvs_list')
