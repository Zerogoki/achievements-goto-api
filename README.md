**На текущий момент реализованы следующие функции**
* `/api/get/achievements` получить список ачивок вместе с подробными данными о них
* `/api/get/user` получить список id ачивок юзера
* `/api/add/achievement` добавить новую ачивку в систему
* `/api/add/user` добавить нового юзера из ядра
* `/api/del/achievement` удалить ачивку из системы
* `/api/del/user` удалить юзера из системы
* `/api/add` присвоить юзеру ачивку
* `/api/del` отнять ачивку у юзера

**Описание запросов к функциям:**
Получение списка ачивок:
`GET /api/get/achievements`

Будет возвращён JSON следующей структуры `{"achivements": [[{"id": 1, "title": "test1", "desc": "Test achievement2", "image": "http://example.org/picture1.jpg"}, {"id": 2, "title": "test2", "desc": "test testestst", "image": "none"}]}]}`

**Для следующих методов требуется ключ доступа** (`access_key`)
Добавление юзера:
`GET /api/add/user?access_key=<key>&id=<id в ядре>&first_name=<Имя>&last_name=<Фамилия>`

Получение списка ачивок юзера
`GET /api/get/user?access_key=<key>&id=<id>`

ВНИМАНИЕ: id в системе ачивок знать не нужно, нужен id в ядре

Пример вывода `{"count": 2, "achievements": [1, 2]}`

Добавление ачивки к юзеру
`GET /api/add?access_key=<key>&user_id=<id юзера в ядре>&ach_id=<id ачивки>`

В случае ошибки возвращается ответ с кодом 403 и описанием ошибки в теле
